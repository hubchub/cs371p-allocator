// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>


using namespace std;
// ---------
// Sentinel
// ---------

// Struct that allows us to interpret addresses as "Sentinels"
// Allows us to use methods and maintain a size
// Example of format : [Header Sentinel][Memory Chunk][Footer Sentinel]
struct Sentinel {
    int sz;
    Sentinel* getFooter() {
        return (Sentinel *) (((char*)( this + 1))  + abs(this->sz));
    }
    Sentinel* getHeader() {
        return ((Sentinel*)(((char*) this) - abs(this->sz)))-1;
    }
    void toggleUsed() {
        this->sz = this->sz*-1;
    }
};


// ---------
// Allocator
// ---------
void allocator_solve(istream& r);

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Iterates over blocks and compares Header Sentinel with Footer Sentinel
     */
    bool valid () const {

        auto b = this->begin();
        auto e = this->end();

        while (b!=e) {


            if (*b != ((Sentinel *)&(*b))->getFooter()->sz)
                return false;

            b++;
        }

        return true;

    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) : _p(p) {
        }

        // ----------
        // operator *
        // ----------
        // Returns Value of address
        int& operator * () const {
            return *this->_p;
        }

        // -----------
        // operator ++
        // -----------
        // Returns iterator to next Header
        iterator& operator ++ () {
            this->_p = (int*) (((Sentinel*)this->_p)->getFooter() + 1);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        // Returns iterator to previous Header
        iterator& operator -- () {

            this->_p = (int *)(((Sentinel*)(this->_p - 1))->getHeader());
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) : _p(p) {}

        // ----------
        // operator *
        // ----------
        // Returns Value of address
        const int& operator * () const {
            return *this->_p;
        }

        // -----------
        // operator ++
        // -----------
        // Returns iterator to next Header
        const_iterator& operator ++ () {
            this->_p = (int*) (((Sentinel*)this->_p)->getFooter() + 1);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        // Returns iterator to previous Header
        const_iterator& operator -- () {

            this->_p = (int *)(((Sentinel*)(this->_p - 1))->getHeader());
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    // Constructs initial Sentinels
    my_allocator () {
        ((Sentinel *)(&(*this)[0]))->sz = N - 8;
        ((Sentinel *)(&(*this)[N-4]))->sz = N - 8;


        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // Prints our output
    void printMe() {
        iterator b = this->begin();
        iterator e = this->end();
        cout << *b;
        b++;
        while (b!=e) {
            cout << " " << *b;
            b++;
        }
        cout << endl;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */

    // finds next fitting and available block
    pointer allocate (size_type n) {
        bad_alloc exception;
        if (n <= 0) {

            throw exception;
        }

        iterator b = this->begin();
        iterator e = this->end();

        while (b!=e) {
            int sz_T = n*sizeof(T);
            if (*b > 0 && *b >= sz_T) {
                Sentinel* head = (Sentinel *)&(*b);
                Sentinel* foot = head->getFooter();
                if (*b >= sz_T + 16) {
                    Sentinel* newFoot = (Sentinel*)((char*)(head+1) + sz_T);
                    Sentinel* newHead = newFoot + 1;
                    newHead->sz = head->sz - (sz_T + 8);
                    foot->sz = newHead->sz;
                    head->sz = -1 * sz_T;
                    newFoot->sz = head->sz;

                }
                else {
                    head->toggleUsed();
                    foot->toggleUsed();
                }
                return (pointer)(head + 1);
            }
            b++;
        }
        throw(exception);
        assert(valid());
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    // gets address of nth block and calls deallocate
    void freeNthBlock(int n) {
        iterator b = this->begin();
        iterator e = this->end();
        int count = 0;
        while (b!=e) {
            if (*b < 0) {
                count++;
                if (count == n) {
                    this->deallocate((pointer)(&(*b)+1),*b);
                }
            }
            b++;
        }
    }
    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * changes Header and Footer Sentinels to positive size
     * checks neighbors and potentially coallesces
     */
    void deallocate (pointer p, size_type n) {
        Sentinel* head = ((Sentinel*)p) - 1;
        head->toggleUsed();
        head->getFooter()->toggleUsed();
        if (head->getFooter() != (Sentinel *)&a[N-4]) {
            Sentinel* right = head->getFooter() + 1;

            if (right->sz > 0) {
                head->sz = head->sz + right->sz + 8;
                right->getFooter()->sz = head->sz;
            }
        }
        if (head != (Sentinel *)&a[0]) {
            Sentinel* left = (head - 1)->getHeader();
            if (left->sz > 0) {
                left->sz = left->sz + head->sz + 8;
                head->getFooter()->sz = left->sz;
            }
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(reinterpret_cast<const int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(reinterpret_cast<const int*>(&a[N]));
    }
};

#endif // Allocator_h
