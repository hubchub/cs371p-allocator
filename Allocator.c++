// -----------
// Allocator.c++
// -----------


// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
#include <sstream>
#include <string>

#include "Allocator.h"

using namespace std;

void allocator_solve(istream& r) {

    string readBuffer;
    int testCases = 0;

    if(getline(r, readBuffer)) {
        testCases = stoi(readBuffer);
    }

    //w >> testCases;


    getline(r, readBuffer); //blank line

    for(int i = 0; i < testCases; ++i) {
        my_allocator<double, 1000> x;
        while(getline(r, readBuffer) && !readBuffer.empty()) {
            int action = stoi(readBuffer);
            if(action >= 0) {
                x.allocate(action);
            }
            else {
                x.freeNthBlock(abs(action));
            }

        }
        x.printMe();
        //string toRet = x.printYourself;
        //w << toRet;
    }
}


