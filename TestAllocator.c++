// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <sstream>
#include "gtest/gtest.h"
#include <string>
#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

template <typename A>
struct TestAllocator2 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator2, my_types_2);

TYPED_TEST(TestAllocator2, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator2, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(TestAllocator2, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(TestAllocator2, test_5) {
    typedef typename TestFixture::allocator_type allocator_type;

    allocator_type   x;
    //TEST SENTINEL CONSTRUCTION
    ASSERT_EQ(x[0], ((Sentinel*) &x[0]) -> sz);
    ASSERT_EQ(92, ((Sentinel*) &x[0]) -> getFooter() -> sz);
}

TYPED_TEST(TestAllocator2, test_6) {
    my_allocator<double, 100> x;
    double* p1 = x.allocate(10);
    //TEST ALLOCATE METHOD
    ASSERT_EQ(((Sentinel*) &x[0]) -> sz, -92);
}

TYPED_TEST(TestAllocator2, test_7) {
    my_allocator<double, 1000> x;
    double* p1 = x.allocate(1);
    auto itr = begin(x);
    ++itr;
    --itr;
    //TEST ++ AND -- ON ITERATOR
    ASSERT_EQ(*itr, -8);
}

TYPED_TEST(TestAllocator2, test_8) {
    my_allocator<double, 1000> x;
    my_allocator<double, 1000>::iterator b = begin(x);
    auto copy = begin(x);

    //TEST BEGIN METHOD, AS WELL AS == ON ITERATOR
    ASSERT_EQ(b, copy);
}

TYPED_TEST(TestAllocator2, test_9) {
    const my_allocator<double, 1000> x;

    auto end_itr = end(x);
    auto b = begin(x);

    //TEST END METHOD AND AS WELL AS != ON ITERATOR
    ASSERT_EQ(end_itr != b, true);
}

TYPED_TEST(TestAllocator2, test_10) {
    my_allocator<double, 1000> x;

    my_allocator<double, 1000>::iterator b = begin(x);

    //TEST * OPERATOR ON ITERATOR
    ASSERT_EQ(*b, 992);
}

TYPED_TEST(TestAllocator2, test_11) {
    my_allocator<double, 1000> x;
    //TESTING ALLOCATE
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
}

TYPED_TEST(TestAllocator2, test_12) {
    my_allocator<double, 1000> x;
    //TESTING ALLOCATE
    x.allocate(124);
    ASSERT_EQ(x[0], -992);
}

TYPED_TEST(TestAllocator2, test_13) {
    my_allocator<double, 1000> x;
    //TESTING DEALLOCATE
    double* p1 = x.allocate(50);
    double* p2 = x.allocate(25);
    double* p3 = x.allocate(10);
    x.deallocate(p2, 25 * sizeof(double));
    Sentinel* headToCheck = (((Sentinel*) p2) - 1);
    ASSERT_EQ(headToCheck -> sz, 25 * sizeof(double));
}

TYPED_TEST(TestAllocator2, test_14) {
    my_allocator<double, 1000> x;
    //TESTING DEALLOCATE
    double* p1 = x.allocate(50);
    double* p2 = x.allocate(25);
    double* p3 = x.allocate(10);
    x.deallocate(p2, 25 * sizeof(double));
    x.deallocate(p3, 50 * sizeof(double));
    x.deallocate(p1, 10 * sizeof(double));
    ASSERT_EQ(x[0], 992);
}

TYPED_TEST(TestAllocator2, test_15) {
    my_allocator<double, 1000> x;
    //TESTING DEALLOCATE
    double* p1 = x.allocate(50);
    x.freeNthBlock(1);
    ASSERT_EQ(x[0], 992);
}

TYPED_TEST(TestAllocator2, test_16) {
    istringstream sin("4\n\n5\n\n5\n3\n\n5\n3\n-1\n\n5\n3\n3\n-1\n-1");
    allocator_solve(sin);
    ASSERT_EQ(1, 1);
}




















